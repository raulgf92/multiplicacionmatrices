#!/usr/bin/env python
# -*- coding: utf-8 -*-
from random import randint
import time
import sys
import json

print "Multiplicacion de matrices cuadradas aleatorias"
print "made by RAULGF92"
print "-------------------------------------"
print " arguments:"
print "[1]:Numero de veces de repetición"
print "[2]:Dimensión inicial de comienzo de las matrices"
print "[3]:Maxima dimensión de las matrices"
print "[4]:Lugar donde imprimir el resultado"
print "-------------------------------------------"
print ""

n=len(sys.argv)
print "Argumentos enviados por el usuario"
print "Número de parámetros: ",n 
print "Lista de argumentos: ", sys.argv
if(n<5 or n>5):
	print "argumentos incorrectos"
	exit(0)

#Admision de argumentos por consola

numberOfMatrix=int(sys.argv[1])
initialDimension=int(sys.argv[2])
maxDimension=int(sys.argv[3])
responseWritePATH=sys.argv[4]

#------------------------------------

def multiplicacionMatriz(A,B,FILA,COLA,FILB,COLB):
	#Suponemos que tienen el mismo rango
	j=0 #X
	resultadoMul=[]
	while(j < FILA):
		vectorAux=[]
		k=0
		while(k < COLB):
			vectorAux.append(0)
			k=k+1
		resultadoMul.append(vectorAux)
		j=j+1
	i=0	
	while(i < FILA):
		j=0
		while(j < COLB):
			k=0
			suma=0
			while(k < COLA):
				auxA=A[i]
				auxB=B[k]
				suma=suma+(auxA[k]*auxB[i])
				k=k+1
			aux=resultadoMul[i]
			aux[j]=suma
			resultadoMul[i]=aux
			j=j+1
		i=i+1

	return resultadoMul
	
	
#Construccion de matrices
def crearMatrizNxN(FIL,COL):
	response=[]	
	i=0
	while(i < FIL):
		vectorAux=[]
		j=0
		while(j < COL):
			vectorAux.append(randint(0,100))
			j=j+1
		i=i+1
		response.append(vectorAux)
	return response


response=[]
INCREMENT=int(maxDimension/numberOfMatrix)
times=0
actualDimension=initialDimension
app_time=time.time();
while(times<numberOfMatrix):

	start_time = time.time()
	A=crearMatrizNxN(actualDimension,actualDimension)
	B=crearMatrizNxN(actualDimension,actualDimension)
	resultado=multiplicacionMatriz(A,B,actualDimension,actualDimension,actualDimension,actualDimension)	
	elapsed_time = time.time() - start_time
	print("matriz multiplicada")
	

	times=times+1
	actualDimension=actualDimension+INCREMENT
	
	#tratamientos para la salida
	dimension=[actualDimension,actualDimension]
	itemResponse={'id':times,'time':elapsed_time,'dimension':dimension,'solution':resultado}
	
	response.append(itemResponse)
	
#Escribimos en el fichero indicado
outfile = open(responseWritePATH+'response.json', 'w') # Indicamos el valor 'w'.
outfile.write(json.dumps({'matrices':response}))
outfile.close()

end_app_time = time.time() - app_time
print("Aplicacion terminada en "+str(end_app_time)+" ms")
	

	

	
		


